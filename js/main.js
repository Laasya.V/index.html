function darkMode(){
    document.body.classList.add("bg-dark");
    var listPTags= document.getElementsByTagName('p');
      for (var i = 0; i < listPTags.length; i++) {
        listPTags[i].classList.remove("text-secondary");
        listPTags[i].classList.add("text-white");
      }

    var headings = document.querySelectorAll("h1, h2, h3, h4");
    for (var i = 0; i < headings.length; i++) {
      headings[i].classList.add("text-white");
    }

    var sections = document.getElementsByTagName("section");
    for (var i = 0; i < sections.length; i++) {
      if(sections[i].classList.contains("bg-light")){
        sections[i].classList.remove("bg-light");
        sections[i].classList.add("bg-wdark");
    }
  }
  }

